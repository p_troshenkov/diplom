package ru.edu.project.authorization;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.api.user.UserService;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class FrontendUserServiceTest {

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserService userService;

    @InjectMocks
    private FrontendUserService frontendUserService;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void insertRow() {
        String username = "username";
        String password = "password";
        String role = "role";

        when(passwordEncoder.encode(password)).thenReturn(password);
        when(userService.register(any(UserInfo.class))).thenAnswer(invocationOnMock -> {
            UserInfo info = invocationOnMock.getArgument(0, UserInfo.class);
            assertEquals(username, info.getUsername());
            assertEquals(password, info.getPassword());
            assertEquals(role, info.getRoles());
            assertTrue(info.getEnabled());
            return 1L;
        });
        Long insertedRows = frontendUserService.insertRow(username, password, role);

        assertEquals(Long.valueOf(1), insertedRows);

    }

    @Test
    public void loadUserByUsername() {
        Long id = 123L;
        String username = "username";
        String password = "password";
        String role = "role";

        UserInfo info = UserInfo.builder()
                .username(username)
                .id(id)
                .password(password)
                .roles(role)
                .build();

        when(userService.loadUserByUsername(username)).thenReturn(info);

        UserDetailsId userDetails = (UserDetailsId) frontendUserService.loadUserByUsername(username);


        assertEquals(id, Long.valueOf(userDetails.getUserId()));
        assertEquals(username, userDetails.getUsername());
        assertEquals(password, userDetails.getPassword());
        assertEquals(AuthorityUtils.createAuthorityList(role).stream().collect(Collectors.toList())
                , userDetails.getAuthorities().stream().collect(Collectors.toList()));

    }

    @Test (expected = UsernameNotFoundException.class)
    public void loadUserByUsernameException() {
        Long id = -123L;
        String username = "username";
        String password = "password";
        String role = "role";

        UserInfo info = UserInfo.builder()
                .username(username)
                .id(id)
                .password(password)
                .roles(role)
                .build();

        when(userService.loadUserByUsername(username)).thenReturn(info);

        UserDetailsId userDetails = (UserDetailsId) frontendUserService.loadUserByUsername(username);

    }
}