package ru.edu.project.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class GroupControllerTest {

    @Mock
    private GroupService groupService;

    @Mock
    private StudentService studentService;

    @InjectMocks
    private GroupController groupController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void index() {
        Model modelMock = mock(Model.class);
        List<GroupInfo> groupInfoList = new ArrayList<>();
        when(groupService.getAllGroups()).thenReturn(groupInfoList);

        String view = groupController.index(modelMock);
        assertEquals("group/index", view);
        verify(modelMock).addAttribute("groups", groupInfoList);
    }

    @Test
    public void view() {
        Long groupId = 123L;
        GroupInfo groupInfo = GroupInfo.builder().build();
        List<StudentInfo> studentInfoList = new ArrayList<>();

        when(groupService.getDetailedInfo(groupId)).thenReturn(null);

        ModelAndView model = groupController.view(groupId);

        assertEquals("group/idNotFound", model.getViewName());
        assertTrue(model.getStatus().is4xxClientError());


        when(groupService.getDetailedInfo(groupId)).thenReturn(groupInfo);
        when(studentService.getStudentsByGroup(groupId)).thenReturn(studentInfoList);

        model = groupController.view(groupId);

        assertEquals("group/view", model.getViewName());
        assertEquals(groupInfo, model.getModel().get("group"));
        assertEquals(studentInfoList, model.getModel().get("students"));
    }

    @Test
    public void editForm() {
        Long groupId = 123L;
        GroupInfo groupInfo = GroupInfo.builder().build();

        ModelAndView model = groupController.editForm(null);
        assertEquals("group/edit", model.getViewName());
        assertNull(model.getModel().get("group"));

        when(groupService.getDetailedInfo(groupId)).thenReturn(null);
        model = groupController.editForm(groupId);
        assertEquals("group/idNotFound", model.getViewName());
        assertTrue(model.getStatus().is4xxClientError());

        when(groupService.getDetailedInfo(groupId)).thenReturn(groupInfo);
        model = groupController.editForm(groupId);
        assertEquals("group/edit", model.getViewName());
        assertEquals(groupInfo, model.getModel().get("group"));
    }

    @Test
    public void editFormProcessing() {
        GroupController.CreateForm createForm = new GroupController.CreateForm();
        createForm.setName("name");
        createForm.setDescription("description");
        Long groupId = 123L;

        GroupInfo groupInfo = GroupInfo.builder().groupId(groupId).build();

        when(groupService.editGroup(any(GroupForm.class))).thenAnswer(invocationOnMock -> {
            GroupForm form = invocationOnMock.getArgument(0, GroupForm.class);
            assertEquals(createForm.getName(), form.getName());
            assertEquals(createForm.getDescription(), form.getDescription());
            return groupInfo;
        });

        String viewName = groupController.editFormProcessing(createForm, groupId);
        assertEquals("redirect:/group/?edited=" + groupInfo.getGroupId(), viewName);
        verify(groupService, times(1)).editGroup(any(GroupForm.class));

        viewName = groupController.editFormProcessing(createForm, null);
        assertEquals("redirect:/group/?created=" + groupInfo.getGroupId(), viewName);
        verify(groupService, times(2)).editGroup(any(GroupForm.class));

    }
}
