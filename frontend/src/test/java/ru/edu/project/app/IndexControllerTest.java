package ru.edu.project.app;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.edu.project.authorization.FrontendUserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class IndexControllerTest {

    public static final SimpleGrantedAuthority ROLE_DEAN = new SimpleGrantedAuthority("ROLE_DEAN");
    public static final SimpleGrantedAuthority ROLE_TEACHER = new SimpleGrantedAuthority("ROLE_TEACHER");
    public static final SimpleGrantedAuthority ROLE_STUDENT = new SimpleGrantedAuthority("ROLE_STUDENT");
    public static final SimpleGrantedAuthority ROLE_ANOTHER = new SimpleGrantedAuthority("ROLE_ANOTHER");

    @Mock
    private FrontendUserService userService;

    @InjectMocks
    private IndexController indexController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void index() {
        Authentication authentication = mock(Authentication.class);
        List<SimpleGrantedAuthority> auth = new ArrayList<>();

        String view = indexController.index(null);
        assertEquals("index", view);

        when(authentication.isAuthenticated()).thenReturn(false);
        view = indexController.index(authentication);
        assertEquals("index", view);


        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getAuthorities())
                .thenAnswer(invocationOnMock -> auth);

        auth.add(ROLE_DEAN);
        view = indexController.index(authentication);
        assertEquals("redirect:/student/", view);

        auth.clear();
        auth.add(ROLE_TEACHER);
        view = indexController.index(authentication);
        assertEquals("redirect:/task/", view);

        auth.clear();
        auth.add(ROLE_STUDENT);
        view = indexController.index(authentication);
        assertEquals("redirect:/report/", view);

        auth.clear();
        auth.add(ROLE_ANOTHER);
        view = indexController.index(authentication);
        assertEquals("index", view);
    }

    @Test
    public void register() {
        String view = indexController.register();
        assertEquals("register", view);
    }

    @Test
    public void registerProcess() {
        String username = "username";
        String password = "password";
        String password2 = "password2";
        String role;
        String view;

        role = "some_role";
        view = indexController.registerProcess(username, password, password2, role);
        assertEquals("redirect:/register?bad_role", view);

        role = "student";
        view = indexController.registerProcess(username, password, password2, role);
        assertEquals("redirect:/register?student_login_incorrect", view);

        role = "teacher";
        view = indexController.registerProcess(username, password, password2, role);
        assertEquals("redirect:/register?bad_password", view);

        password = password2 = "123";
        view = indexController.registerProcess(username, password, password2, role);
        assertEquals("redirect:/register?bad_password", view);

        password = password2 = "1234";
        role = "teacher";
        view = indexController.registerProcess(username, password, password2, role);
        verify(userService).insertRow(username, password, "ROLE_TEACHER");
        assertEquals("redirect:/login", view);

        role = "dean";
        view = indexController.registerProcess(username, password, password2, role);
        verify(userService).insertRow(username, password, "ROLE_DEAN");
        assertEquals("redirect:/login", view);

        role = "student";
        username = "123";
        view = indexController.registerProcess(username, password, password2, role);
        verify(userService).insertRow(username, password, "ROLE_STUDENT");
        assertEquals("redirect:/login", view);

        when(userService.insertRow(username, password, "ROLE_STUDENT")).thenThrow(new RuntimeException());
        view = indexController.registerProcess(username, password, password2, role);
        assertEquals("redirect:/register?invalid_request", view);

    }
}