package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskService;
import ru.edu.project.backend.stub.group.InMemoryStubGroupService;
import ru.edu.project.backend.stub.students.InMemoryStubStudentService;
import ru.edu.project.backend.stub.tasks.InMemoryStubTaskService;

@Configuration
@Profile("STUBS")
public class StubsConfig {

    /**
     * Заглушка сервиса.

     * @return bean
   */
   @Bean
   public StudentService studentServiceBean() {
        return new InMemoryStubStudentService();
  }

    /**
     * Заглушка сервиса.
     *
     * @return bean
     */
    @Bean
    public TaskService taskServiceBean() {
        return new InMemoryStubTaskService();
    }

    /**
     * Заглушка сервиса.
     * @param studentService
     * @return bean
     */
    @Bean
    public GroupService groupServiceBean(final StudentService studentService) {
        return new InMemoryStubGroupService(studentService);
    }

}
