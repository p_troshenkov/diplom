package ru.edu.project.frontend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.project.authorization.UserDetailsId;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

@RequestMapping("/report")
@Controller
public class ReportController {

    /**
     * Ссылка на сервис студентов.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Ссылка на сервис заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Отображение списка студентов.
     *
     * @param model
     * @param authentication
     * @return path
     */
    @GetMapping("/")
    public String index(final Model model, final Authentication authentication) {
        UserDetailsId userDetailsId = (UserDetailsId) authentication.getPrincipal();
        Long userId = Long.parseLong(userDetailsId.getUsername());
        StudentInfo info = studentService.getDetailedInfo(userId);

        if (info == null) {
            model.addAttribute("error", "");
        }
        model.addAttribute("student", info);

        TaskInfo taskInfo = taskService.getTaskByStudent(userId);
        if (taskInfo != null) {
            model.addAttribute("task", taskInfo);
        }

        return "report/index";
    }
}
