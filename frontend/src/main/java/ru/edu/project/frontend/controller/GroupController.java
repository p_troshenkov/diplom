package ru.edu.project.frontend.controller;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequestMapping("/group")
@Controller
public class GroupController {

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Ссылка на сервис студентов.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Отображение списка групп.
     * @param model
     * @return path
     */
    @GetMapping("/")
    public String index(final Model model) {
        model.addAttribute("groups", groupService.getAllGroups());
        return "group/index";
    }

    /**
     * Отображение информации по группе по номеру.
     * @param groupId
     * @return ModelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView view(final @PathVariable("id") Long groupId) {
        ModelAndView model = new ModelAndView("group/view");

        GroupInfo groupInfo = groupService.getDetailedInfo(groupId);
        if (groupInfo == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("group/idNotFound");
            return model;
        }
        List<StudentInfo> studentInfoList = studentService.getStudentsByGroup(groupId);

        model.addObject("group", groupInfo);
        model.addObject("students", studentInfoList);
        return model;
    }

    /**
     * Отображение формы группы.
     * @param groupId
     * @return ModelAndView
     */
    @GetMapping(value = {"/edit/{groupId}", "/edit"})
    public ModelAndView editForm(final @PathVariable(required = false) Long groupId) {
        ModelAndView model = new ModelAndView("group/edit");

        if (groupId != null) {
            GroupInfo groupInfo = groupService.getDetailedInfo(groupId);
            if (groupInfo == null) {
                model.setStatus(HttpStatus.NOT_FOUND);
                model.setViewName("group/idNotFound");
                return model;
            } else {
                model.addObject("group", groupInfo);
            }
        }
        return model;
    }

    /**
     * Создание/редактирование группы.
     * @param form
     * @param groupId
     * @return path
     */
    @PostMapping(value = {"/edit/{groupId}", "/edit"})
    public String editFormProcessing(
            @Valid
            @ModelAttribute final CreateForm form,
            final @PathVariable(required = false) Long groupId
    ) {
        GroupInfo groupInfo = groupService.editGroup(GroupForm.builder()
                .groupId(groupId)
                .name(form.getName())
                .description(form.getDescription())
                .build());
        if (groupId != null) {
            return "redirect:/group/?edited=" + groupInfo.getGroupId();
        } else {
            return "redirect:/group/?created=" + groupInfo.getGroupId();
        }
    }

    @Getter
    @Setter
    public static class CreateForm {
        /**
         * Название группы.
         */
        @NotNull
        private String name;

        /**
         * Описание группы.
         */
        private String description;
    }
}
