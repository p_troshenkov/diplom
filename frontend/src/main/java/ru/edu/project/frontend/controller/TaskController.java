package ru.edu.project.frontend.controller;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.api.tasks.RequestToMakeAssessment;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Для отладки через spring-boot:run не забываем добавить флаг.
 * -Dspring-boot.run.fork=false
 */
@RequestMapping("/task")
@Controller
public class TaskController {

    /**
     * Атрибут модели для хранения списка доступных заданий.
     */
    public static final String TASKS_ATTR = "tasks";

    /**
     * Ссылка на сервис ввода заданий.
     */
    @Autowired
    private TaskService taskService;

    /**
     * Отображение заданий студента.
     *
     * @return view
     */
    @GetMapping("/")
    public ModelAndView index() {

        ModelAndView model = new ModelAndView("task/index");

        List<TaskInfo> tasks = taskService.getAllTasks();

        model.addObject(TASKS_ATTR, tasks);

        return model;
    }

    /**
     * Просмотр задания по id.
     *
     * @param studentId
     * @return modelAndView
     */
    @GetMapping("/view/{id}")
    public ModelAndView view(final @PathVariable("id") Long studentId) {

        ModelAndView model = new ModelAndView("task/view");

        TaskInfo detailedInfo = taskService.getTaskByStudent(studentId);
        if (detailedInfo == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("task/viewNotFound");
            return model;
        }

        model.addObject(
                "record",
                detailedInfo
        );

        return model;
    }

    /**
     * Отображение формы для создания заявки.
     *
     * @param model
     * @return modelAndView
     */
    @GetMapping("/create")
    public String createForm(final Model model) {
        model.addAttribute(TASKS_ATTR, taskService.getAvailable());
        return "task/create";
    }

    /**
     * Получаем форму с предварительной валидацией.
     *
     * @param form
     * @param model
     * @return redirect url
     */
    @PostMapping("/create")
    public String createFormProcessing(
            @Valid
            @ModelAttribute final CreateForm form,
            final Model model
    ) {

        TaskForm taskForm = TaskForm.builder()
                .studentId(Long.parseLong(form.getStudentId()))
                .deadLine(form.getDeadLine())
                .selectedSubjects(form.getSubjects())
                .comment(form.getComment())
                .build();

        TaskInfo task = taskService.createTask(taskForm);

        return "redirect:/task/?created=" + task.getStudentId();
    }

    @Getter
    @Setter
    public static class CreateForm {

        /**
         * Для парсинга даты.
         */
        private static final DateFormat FORMAT;

        static {
            FORMAT = new SimpleDateFormat("yyyy-MM-dd");
        }

        /**
         * Выбранный номер студента.
         */
        @NotEmpty
        private String studentId;

        /**
         * Выбранные предметы.
         */
        @NotNull
        private List<Long> subjects;

        /**
         * Выбранное время посещения.
         */
        @NotEmpty
        private String deadline;

        /**
         * Описание проблемы.
         */
        private String comment;

        /**
         * Получение объекта календаря с временем посещения.
         *
         * @return календарь
         */
        @SneakyThrows
        public Timestamp getDeadLine() {
            Date parsed = FORMAT.parse(deadline);
            return new Timestamp(parsed.getTime());
        }
    }

    /**
     * Отображение формы для выставления оценок.
     *
     * @param studentId id студента
     * @return modelAndView
     */
    @GetMapping("/assessment/{id}")
    public ModelAndView assessmentForm(@PathVariable("id") final Long studentId) {

        ModelAndView model = new ModelAndView("/task/assessment");

        TaskInfo detailedInfo = taskService.getTaskByStudent(studentId);

        model.addObject("subjects", detailedInfo);
        return model;
    }

    /**
     * Получаем форму с предварительной валидацией.
     *
     * @param form
     * @param studentId
     * @return redirect url
     */
    @PostMapping("/assessment/{id}")
    public String assessmentFormProcessing(
            @Valid
            @ModelAttribute final AssessmentForm form,
            @PathVariable("id") final Long studentId
    ) {

        RequestToMakeAssessment requestForm = RequestToMakeAssessment.builder()
                .studentId(studentId)
                .assessments(new Long[]
                        {
                                Long.parseLong(form.getAssess1()),
                                Long.parseLong(form.getAssess2()),
                                Long.parseLong(form.getAssess3()),
                                Long.parseLong(form.getAssess4())
                        })
                .build();

        taskService.makeAssessment(requestForm);

        return "redirect:/task/?updated=" + studentId;
    }

    @Getter
    @Setter
    public static class AssessmentForm {

        /**
         * Введенная оценка по предмету №1.
         */
        @NotNull
        private String assess1;

        /**
         * Введенная оценка по предмету №2.
         */
        @NotNull
        private String assess2;

        /**
         * Введенная оценка по предмету №3.
         */
        @NotNull
        private String assess3;

        /**
         * Введенная оценка по предмету №4.
         */
        @NotNull
        private String assess4;

    }

}
