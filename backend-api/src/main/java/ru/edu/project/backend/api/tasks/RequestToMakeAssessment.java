package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class RequestToMakeAssessment {

    /**
     * id студента.
     */
    private Long studentId;

    /**
     * Выбранные предметы.
     */
    private Long[] assessments;

}
