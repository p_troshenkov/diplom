package ru.edu.project.backend.api.students;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;


@Getter
@Builder
@Jacksonized
public class StudentForm {

    /**
     * id группы студента.
     */
    private Long groupId;

    /**
     * Номер зачетки.
     */
    private Long recordId;

    /**
     * Имя студента.
     */
    private String name;

    /**
     * Отчество студента.
     */
    private String secondName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * Дата рождения студента.
     */
    private LocalDate birthdayDate;

    /**
     * Контактный номер телефона.
     */
    private String phone;
}
