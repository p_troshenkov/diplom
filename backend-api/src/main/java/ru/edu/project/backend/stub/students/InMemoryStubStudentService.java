package ru.edu.project.backend.stub.students;

import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.api.students.StudentService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;


public class InMemoryStubStudentService implements StudentService {

    /**
     * Для выделения года.
     */
    public static final int CENTURY = 100;

    /**
     * Локальное хранилище данных в RAM.
     */
    private Map<Long, StudentInfo> db = new ConcurrentHashMap<>();

    /**
     * Локальный счетчик студентов.
     */
    private AtomicLong idCount = new AtomicLong(0);

    /**
     * Добавление данных студента.
     *
     * @param studentForm
     * @return StudentInfo
     */
    @Override
    public StudentInfo editStudent(final StudentForm studentForm) {
        long idInGroup = getStudentsByGroup(studentForm.getGroupId()).size() + 1;
        Long tempRecordId;
        if (studentForm.getRecordId() != null) {
            tempRecordId = studentForm.getRecordId();
        } else {
            tempRecordId = createRecordId(studentForm.getGroupId(), idInGroup);
        }

        StudentInfo info = StudentInfo.builder()
                .groupId(studentForm.getGroupId())
                .recordId(tempRecordId)
                .name(studentForm.getName())
                .secondName(studentForm.getSecondName())
                .lastName(studentForm.getLastName())
                .birthdayDate(studentForm.getBirthdayDate())
                .phone(studentForm.getPhone())
                .entryDate(LocalDate.now())
                .build();


        db.put(info.getRecordId(), info);
        return info;
    }

    /**
     * Просмотр информации о студенте.
     *
     * @param recordId
     * @return StudentInfo
     */
    @Override
    public StudentInfo getDetailedInfo(final Long recordId) {
        if (db.containsKey(recordId)) {
            return db.get(recordId);
        } else {
            return null;
        }
    }

    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    public List<StudentInfo> getAllStudents() {
        return new ArrayList<StudentInfo>(db.values());
    }

    /**
     * Вывод всех студентов по номеру группы.
     *
     * @param groupId
     * @return List
     */
    @Override
    public List<StudentInfo> getStudentsByGroup(final Long groupId) {
        List<StudentInfo> students = null;
        if (db != null) {
            students = db.values().stream()
                    .filter(x -> x.getGroupId() == groupId)
                    .collect(Collectors.toList());
        }
        return students;
    }

    /**
     * Удаление студента.
     *
     * @param recordId
     */
    @Override
    public void deleteStudent(final Long recordId) {
        db.remove(recordId);
    }

    /**
     * Генерация номера зачетки студента.
     * @param groupId
     * @param idInGroup
     * @return Long
     */
    public Long createRecordId(final Long groupId, final Long idInGroup) {
        String recordId = String.valueOf(LocalDate.now().getYear() % CENTURY)
                + String.format("%03d%03d", groupId, idInGroup);
        return Long.parseLong(recordId);
    }
}
