package ru.edu.project.backend.api.tasks;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Builder
@Jacksonized
public class TaskForm {

    /**
     * id студента.
     */
    private Long studentId;

    /**
     * Дата сдачи контрольных работ.
     */
    private Timestamp deadLine;

    /**
     * Выбранные предметы.
     */
    private List<Long> selectedSubjects;

    /**
     * Комментарий.
     */
    private String comment;

}
