package ru.edu.project.backend.api.groups;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface GroupService {
    /**
     * Добавление/редактирование данных группы.
     * @param groupForm
     * @return StudentInfo
     */
    @AcceptorArgument
    GroupInfo editGroup(GroupForm groupForm);

    /**
     * Просмотр информации о группе.
     * @param groupId
     * @return StudentInfo
     */
    GroupInfo getDetailedInfo(Long groupId);

    /**
     * Вывод всех групп.
     * @return List
     */
    List<GroupInfo> getAllGroups();

    /**
     * Инкремент количества студентов.
     * @param groupId
     * @return Long
     */
    Long addAndGetStudentsCount(Long groupId);
}
