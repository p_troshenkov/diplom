package ru.edu.project.backend.stub.tasks;

import ru.edu.project.backend.api.common.StatusImpl;
import ru.edu.project.backend.api.tasks.RequestToMakeAssessment;
import ru.edu.project.backend.api.tasks.Subject;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.api.tasks.TaskService;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;


public class InMemoryStubTaskService implements TaskService {

    /**
     * Локальное хранилище заданий в RAM.
     * Long: номер студента
     * List<TaskInfo>: список предметов с оценками
     */
    private Map<Long, TaskInfo> db = new ConcurrentHashMap<>();

    /**
     * Локальный счетчик заданий.
     */
    private AtomicLong idCount = new AtomicLong(0);

    /**
     * @inheritDoc
     */
    @Override
    public List<Subject> getAvailable() {
        return Arrays.stream(SubjectsEnum.values())
                .map(SubjectsEnum::getSubject)
                .collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<Subject> getByIds(final List<Long> ids) {

        List<Subject> subjectList = Arrays.stream(SubjectsEnum.values())
                .filter(e -> ids.contains(e.getSubject().getId()))
                .map((subjectsEnum) -> {
                    Subject s = subjectsEnum.getSubject();

                    Subject newSubject = new Subject();
                    newSubject.copy(s);

                    return newSubject;
                })
                .collect(Collectors.toList());

        return subjectList;
    }


    public enum SubjectsEnum {

        /**
         * Отладочная услуга.
         */
        JOB_1(1L, "Слетать на Луну"),

        /**
         * Отладочная услуга.
         */
        JOB_2(2L, "Позвонить подруге"),

        /**
         * Отладочная услуга.
         */
        JOB_3(3L, "Подтянуться 20 раз"),

        /**
         * Отладочная услуга.
         */
        JOB_4(4L, "Выпить кофе");


        /**
         * Связанный объект.
         */
        private Subject subject;

        SubjectsEnum(final Long code, final String title) {
            Subject newSubject = new Subject();
            newSubject.setId(code);
            newSubject.setTitle(title);

            subject = newSubject;
        }

        /**
         * Получение объекта.
         *
         * @return Subject
         */
        public Subject getSubject() {
            return subject;
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public TaskInfo createTask(final TaskForm taskForm) {

        TaskInfo info = TaskInfo.builder()
                .id(idCount.addAndGet(1))
                .studentId(taskForm.getStudentId())
                .createdAt(new Timestamp(new Date().getTime()))
                .deadLine(taskForm.getDeadLine())
                .status(StatusImpl.builder()
                        .code(1L)
                        .message("Созданo")
                        .build())
                .comment(taskForm.getComment())
                .subjects(getSubjectsById(taskForm))
                .build();

        if (!db.containsKey(taskForm.getStudentId())) {
            db.put(taskForm.getStudentId(), info);
        }

        return info;
    }

    private List<Subject> getSubjectsById(final TaskForm requestForm) {
        return this.getByIds(requestForm.getSelectedSubjects());
    }

    /**
     * @inheritDoc
     */
    @Override
    public TaskInfo getTaskByStudent(final long id) {
        if (db.containsKey(id)) {
            return db.get(id);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<TaskInfo> getAllTasks() {
        if (!db.isEmpty()) {

            return new ArrayList<>(db.values());
        }
        return Collections.emptyList();
    }

    /**
     * @param assessmentsList
     * @return
     */
    @Override
    public TaskInfo makeAssessment(final RequestToMakeAssessment assessmentsList) {

        Long studentId = assessmentsList.getStudentId();

        if (!db.containsKey(studentId)) {
            return null;
        }

        TaskInfo info = db.get(studentId);

        List<Subject> subjectList = info.getSubjects();
        int i = 0;
        for (Subject subject : subjectList) {
            subject.setAssessment(assessmentsList.getAssessments()[i++]);
        }

        TaskInfo newInfo = TaskInfo.builder()
                .id(info.getId())
                .studentId(info.getStudentId())
                .createdAt(info.getCreatedAt())
                .deadLine(info.getDeadLine())
                .status(StatusImpl.builder()
                        .code(2L)
                        .message("Здано")
                        .build())
                .comment(info.getComment())
                .subjects(subjectList)
                .build();

        db.put(studentId, newInfo);

        return newInfo;
    }
}
