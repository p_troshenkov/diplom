package ru.edu.project.backend.api.groups;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
public class GroupInfo {
    /**
     * id группы.
     */
    private Long groupId;

    /**
     * Название группы.
     */
    private String name;

    /**
     * Описание.
     */
    private String description;

    /**
     * количество зарегестрированных студентов.
     */
    private Long studentsCount;
}
