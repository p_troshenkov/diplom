package ru.edu.project.backend.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;

import java.util.List;

@Repository
public interface GroupEntityRepository extends CrudRepository<GroupEntity, Long> {

    /**
     * Поиск записей по полю group_id.
     *
     * @param groupId
     * @return list entity
     */
    List<GroupEntity> findByGroupId(Long groupId);
}
