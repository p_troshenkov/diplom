package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.service.GroupServiceLayer;

import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController implements GroupService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private GroupServiceLayer delegate;

    /**
     * Добавление/редактирование данных группы.
     *
     * @param groupForm
     * @return StudentInfo
     */
    @Override
    @PostMapping("/editGroup")
    public GroupInfo editGroup(@RequestBody final GroupForm groupForm) {
        return delegate.editGroup(groupForm);
    }

    /**
     * Просмотр информации о группе.
     *
     * @param groupId
     * @return StudentInfo
     */
    @Override
    @GetMapping("/getDetailedInfo/{groupId}")
    public GroupInfo getDetailedInfo(@PathVariable("groupId") final Long groupId) {
        return delegate.getDetailedInfo(groupId);
    }

    /**
     * Вывод всех групп.
     *
     * @return List
     */
    @Override
    @GetMapping("/getAllGroups")
    public List<GroupInfo> getAllGroups() {
        return delegate.getAllGroups();
    }

    /**
     * Инкремент количества студентов.
     *
     * @param groupId
     * @return Long
     */
    @Override
    public Long addAndGetStudentsCount(final Long groupId) {
        return delegate.addAndGetStudentsCount(groupId);
    }
}
