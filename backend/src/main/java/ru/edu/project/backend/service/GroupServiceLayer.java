package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.GroupForm;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.groups.GroupService;
import ru.edu.project.backend.da.GroupDALayer;

import java.util.List;

@Service
@Profile("!STUB")
@Qualifier("GroupServiceLayer")
public class GroupServiceLayer implements GroupService {

    /**
     * Зависимость для слоя доступа к данным групп.
     */
    @Autowired
    private GroupDALayer daLayer;

    /**
     * Добавление/редактирование данных группы.
     *
     * @param groupForm
     * @return StudentInfo
     */
    @Override
    public GroupInfo editGroup(final GroupForm groupForm) {
        GroupInfo draft = GroupInfo.builder()
                .name(groupForm.getName())
                .description(groupForm.getDescription())
                .groupId(groupForm.getGroupId())
                .build();
        return daLayer.save(draft);
    }

    /**
     * Просмотр информации о группе.
     *
     * @param groupId
     * @return StudentInfo
     */
    @Override
    public GroupInfo getDetailedInfo(final Long groupId) {
        return daLayer.getById(groupId);
    }

    /**
     * Вывод всех групп.
     *
     * @return List
     */
    @Override
    public List<GroupInfo> getAllGroups() {
        return daLayer.getAll();
    }

    /**
     * Инкремент количества студентов.
     */
    @Override
    public Long addAndGetStudentsCount(final Long groupId) {
        GroupInfo draft = getDetailedInfo(groupId);
        Long studentCount = draft.getStudentsCount() + 1;
        draft.setStudentsCount(studentCount);
        daLayer.save(draft);
        return studentCount;
    }
}
