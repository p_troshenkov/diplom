package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.da.StudentDALayer;
import ru.edu.project.backend.da.jpa.converter.StudentMapper;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;
import ru.edu.project.backend.da.jpa.repository.StudentEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPAStudentDA implements StudentDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private StudentEntityRepository repo;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private StudentMapper mapper;

    /**
     * Получение данных студента по номеру зачетки.
     *
     * @param recordId
     * @return student
     */
    @Override
    public StudentInfo getById(final Long recordId) {
        Optional<StudentEntity> entity = Optional.ofNullable(repo.findStudentEntityByRecordId(recordId));
        return entity.map(studentEntity -> mapper.map(studentEntity)).orElse(null);
    }

    /**
     * Сохранение (создание/обновление) данных студента.
     *
     * @param draft
     * @return student
     */
    @Override
    public StudentInfo save(final StudentInfo draft) {
        StudentEntity entity = mapper.map(draft);

        StudentEntity saved = repo.save(entity);

        return mapper.map(saved);
    }

    /**
     * Получение списка студентов.
     *
     * @return list students
     */
    @Override
    public List<StudentInfo> getAll() {
        return mapper.mapList(repo.findAll());
    }

    /**
     * Получение списка студентов по номеру группы.
     *
     * @param groupId
     * @return list students
     */
    @Override
    public List<StudentInfo> getStudentsByGroup(final Long groupId) {
        return mapper.mapList(repo.findByGroupId(groupId));
    }

    /**
     * Удаление данных студента.
     *
     * @param recordId
     */
    @Override
    public void deleteStudentById(final Long recordId) {
        repo.deleteById(recordId);
    }
}
