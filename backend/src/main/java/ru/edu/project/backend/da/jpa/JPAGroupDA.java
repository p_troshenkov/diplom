package ru.edu.project.backend.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.da.GroupDALayer;
import ru.edu.project.backend.da.jpa.converter.GroupMapper;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.repository.GroupEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@Profile("SPRING_DATA")
public class JPAGroupDA implements GroupDALayer {

    /**
     * Зависимость на репозиторий.
     */
    @Autowired
    private GroupEntityRepository repo;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private GroupMapper mapper;

    /**
     * Получение данных группы по номеру.
     *
     * @param groupId
     * @return student
     */
    @Override
    public GroupInfo getById(final Long groupId) {
        Optional<GroupEntity> entity = repo.findById(groupId);
        return entity.map(groupEntity -> mapper.map(groupEntity)).orElse(null);
    }

    /**
     * Сохранение (создание/обновление) группы.
     *
     * @param draft
     * @return group
     */
    @Override
    public GroupInfo save(final GroupInfo draft) {
        GroupEntity entity = mapper.map(draft);
        if (entity.getStudentsCount() == null) {
            entity.setStudentsCount(0L);
        }

        GroupEntity saved = repo.save(entity);
        draft.setGroupId(saved.getGroupId());
        return mapper.map(saved);
    }

    /**
     * Получение списка групп.
     *
     * @return list groups
     */
    @Override
    public List<GroupInfo> getAll() {
        return mapper.mapList(repo.findAll());
    }
}
