package ru.edu.project.backend.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import ru.edu.project.backend.api.groups.GroupAbstract;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "students_group")
public class GroupEntity implements GroupAbstract {
    /**
     * group_id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "group_seq")
    @SequenceGenerator(name = "group_seq", sequenceName = "group_id_sequence", allocationSize = 1)
    @Column (name = "group_id")
    private Long groupId;

    /**
     * name.
     */
    private String name;

    /**
     * description.
     */
    private String description;

    /**
     * количество зарегестрированных студентов.
     */
    @Column (name = "students_count")
    private Long studentsCount;

}
