package ru.edu.project.backend.da.jpa.converter;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {
    /**
     * Маппер StudentInfo -> StudentEntity.
     *
     * @param studentInfo
     * @return entity
     */
    StudentEntity map(StudentInfo studentInfo);

    /**
     * Маппер StudentEntity -> StudentInfo.
     *
     * @param entity
     * @return studentInfo
     */
    StudentInfo map(StudentEntity entity);

    /**
     * Маппер List<StudentEntity> -> List<StudentInfo>.
     *
     * @param listEntity
     * @return list studentInfo
     */
    List<StudentInfo> mapList(Iterable<StudentEntity> listEntity);
}
