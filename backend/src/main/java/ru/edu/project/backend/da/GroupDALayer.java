package ru.edu.project.backend.da;

import ru.edu.project.backend.api.groups.GroupInfo;

import java.util.List;

public interface GroupDALayer {
    /**
     * Получение данных группы по номеру.
     *
     * @param groupId
     * @return student
     */
    GroupInfo getById(Long groupId);

    /**
     * Сохранение (создание/обновление) группы.
     *
     * @param draft
     * @return group
     */
    GroupInfo save(GroupInfo draft);

    /**
     * Получение списка групп.
     *
     * @return list groups
     */
    List<GroupInfo> getAll();
}
