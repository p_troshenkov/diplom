package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.da.jpa.converter.GroupMapper;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.repository.GroupEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPAGroupDATest {

    @Mock
    private GroupEntityRepository repo;

    @Mock
    private GroupMapper mapper;

    @InjectMocks
    private JPAGroupDA groupDA;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getById() {
        Long groupId = 123L;
        GroupInfo info = GroupInfo.builder().build();
        GroupEntity entity = new GroupEntity();
        when(repo.findById(groupId)).thenReturn(Optional.of(entity));
        when(mapper.map(entity)).thenReturn(info);

        assertEquals(info, groupDA.getById(groupId));
        verify(repo).findById(groupId);
        verify(mapper).map(entity);

        when(repo.findById(groupId)).thenReturn(Optional.empty());
        assertNull(groupDA.getById(groupId));
    }

    @Test
    public void save() {
        GroupInfo draft = GroupInfo.builder().build();
        GroupEntity entity = new GroupEntity();

        when(mapper.map(draft)).thenReturn(entity);
        when(repo.save(entity)).thenReturn(entity);
        when(mapper.map(entity)).thenReturn(draft);

        entity.setGroupId(2L);

        GroupInfo test = groupDA.save(draft);
        assertEquals(entity.getGroupId(), test.getGroupId());
        verify(mapper).map(any(GroupInfo.class));
        verify(mapper).map(any(GroupEntity.class));
        verify(repo).save(any(GroupEntity.class));

        entity.setStudentsCount(1L);
        test = groupDA.save(draft);
        assertEquals(entity.getGroupId(), test.getGroupId());

    }

    @Test
    public void getAll() {
        List<GroupEntity> groupEntityList = new ArrayList<>();
        List<GroupInfo> groupInfoList = new ArrayList<>();
        when(repo.findAll()).thenReturn(groupEntityList);
        when(mapper.mapList(groupEntityList)).thenReturn(groupInfoList);

        assertEquals(groupInfoList, groupDA.getAll());
        verify(repo).findAll();
        verify(mapper).mapList(anyList());
    }
}