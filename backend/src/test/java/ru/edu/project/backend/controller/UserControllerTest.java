package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.da.UserDALayer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class UserControllerTest {
    @Mock
    private UserDALayer userServiceDA;

    @InjectMocks
    private UserController userController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void register() {
        Long id = 123L;
        UserInfo user = UserInfo.builder().build();
        UserInfo userRegistered = UserInfo.builder()
                .id(id)
                .build();
        when(userServiceDA.register(user)).thenReturn(userRegistered);

        assertEquals(id, userController.register(user));
        verify(userServiceDA).register(user);
    }

    @Test (expected = IllegalStateException.class)
    public void registerException() {
        UserInfo user = UserInfo.builder().build();

        when(userServiceDA.register(user)).thenReturn(null);
        userController.register(user);
        verify(userServiceDA).register(user);
    }

    @Test
    public void loadUserByUsername() {
        UserInfo EMPTY_USER = UserInfo.builder().id(-1L).build();
        UserInfo user = UserInfo.builder().build();
        String username = "username";

        when(userServiceDA.findByUsername(username)).thenReturn(null);
        assertEquals(EMPTY_USER.getId(), userController.loadUserByUsername(username).getId());
        verify(userServiceDA).findByUsername(username);

        when(userServiceDA.findByUsername(username)).thenReturn(user);
        assertEquals(user, userController.loadUserByUsername(username));
    }
}