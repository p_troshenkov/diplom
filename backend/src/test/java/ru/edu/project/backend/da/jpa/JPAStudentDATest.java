package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.groups.GroupInfo;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.da.jpa.converter.StudentMapper;
import ru.edu.project.backend.da.jpa.entity.GroupEntity;
import ru.edu.project.backend.da.jpa.entity.StudentEntity;
import ru.edu.project.backend.da.jpa.repository.StudentEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPAStudentDATest {

    @Mock
    private StudentEntityRepository repo;

    @Mock
    private StudentMapper mapper;

    @InjectMocks
    private JPAStudentDA studentDA;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getById() {
        Long recordId = 123L;
        StudentInfo info = StudentInfo.builder().build();
        StudentEntity entity = new StudentEntity();
        when(repo.findStudentEntityByRecordId(recordId)).thenReturn(entity);
        when(mapper.map(entity)).thenReturn(info);

        assertEquals(info, studentDA.getById(recordId));
        verify(repo).findStudentEntityByRecordId(recordId);
        verify(mapper).map(entity);

        when(repo.findStudentEntityByRecordId(recordId)).thenReturn(null);
        assertNull(studentDA.getById(recordId));

    }

    @Test
    public void save() {
        StudentInfo draft = StudentInfo.builder().build();
        StudentEntity entity = new StudentEntity();

        when(mapper.map(draft)).thenReturn(entity);
        when(repo.save(entity)).thenReturn(entity);
        when(mapper.map(entity)).thenReturn(draft);

        assertEquals(draft, studentDA.save(draft));
    }

    @Test
    public void getAll() {
        List<StudentEntity> entityList = new ArrayList<>();
        List<StudentInfo> infoList = new ArrayList<>();

        when(repo.findAll()).thenReturn(entityList);
        when(mapper.mapList(entityList)).thenReturn(infoList);

        assertEquals(infoList, studentDA.getAll());
        verify(repo).findAll();
        verify(mapper).mapList(entityList);
    }

    @Test
    public void getStudentsByGroup() {
        Long groupId = 123L;
        List<StudentEntity> entityList = new ArrayList<>();
        List<StudentInfo> infoList = new ArrayList<>();

        when(repo.findByGroupId(groupId)).thenReturn(entityList);
        when(mapper.mapList(entityList)).thenReturn(infoList);

        assertEquals(infoList, studentDA.getStudentsByGroup(groupId));
        verify(repo).findByGroupId(groupId);
        verify(mapper).mapList(entityList);
    }

    @Test
    public void deleteStudentById() {
        Long recordId = 123L;
        studentDA.deleteStudentById(recordId);

        verify(repo).deleteById(recordId);
    }
}