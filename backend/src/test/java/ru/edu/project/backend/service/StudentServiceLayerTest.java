package ru.edu.project.backend.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.da.StudentDALayer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class StudentServiceLayerTest {

    @Mock
    private StudentDALayer daLayer;

    @Mock
    private GroupServiceLayer groupService;

    @InjectMocks
    private StudentServiceLayer studentServiceLayer;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void editStudent() {
        LocalDate birthday = LocalDate.of(1992, 1, 1);
        StudentForm studentForm = StudentForm.builder()
                .recordId(123L)
                .groupId(456L)
                .name("name")
                .secondName("second_name")
                .lastName("last_name")
                .birthdayDate(birthday)
                .phone("+7-944")
                .build();

        when(daLayer.save(any(StudentInfo.class))).thenAnswer(invocationOnMock -> {
           StudentInfo info = invocationOnMock.getArgument(0, StudentInfo.class);
           assertEquals(studentForm.getRecordId(), info.getRecordId());
           assertEquals(studentForm.getGroupId(), info.getGroupId());
           assertEquals(studentForm.getName(), info.getName());
           assertEquals(studentForm.getSecondName(), info.getSecondName());
           assertEquals(studentForm.getLastName(), info.getLastName());
           assertEquals(studentForm.getPhone(), info.getPhone());
           assertEquals(studentForm.getBirthdayDate(), info.getBirthdayDate());
           return info;

        });
        studentServiceLayer.editStudent(studentForm);
    }

    @Test
    public void editStudentNullId() {
        LocalDate birthday = LocalDate.of(1992, 1, 1);
        StudentForm studentForm = StudentForm.builder()
                .groupId(456L)
                .name("name")
                .secondName("second_name")
                .lastName("last_name")
                .birthdayDate(birthday)
                .phone("+7-944")
                .build();

        Long expectedId = 22456001L;

        when(groupService.addAndGetStudentsCount(studentForm.getGroupId())).thenReturn(1L);

        when(daLayer.save(any(StudentInfo.class))).thenAnswer(invocationOnMock -> {
            StudentInfo info = invocationOnMock.getArgument(0, StudentInfo.class);
            assertEquals(expectedId, info.getRecordId());
            assertEquals(studentForm.getGroupId(), info.getGroupId());
            assertEquals(studentForm.getName(), info.getName());
            assertEquals(studentForm.getSecondName(), info.getSecondName());
            assertEquals(studentForm.getLastName(), info.getLastName());
            assertEquals(studentForm.getPhone(), info.getPhone());
            assertEquals(studentForm.getBirthdayDate(), info.getBirthdayDate());
            return info;

        });
        studentServiceLayer.editStudent(studentForm);
    }

    @Test
    public void getDetailedInfo() {
        StudentInfo info = StudentInfo.builder().build();
        Long recordId = 123L;
        when(daLayer.getById(recordId)).thenReturn(info);

        assertEquals(info, studentServiceLayer.getDetailedInfo(recordId));
        verify(daLayer).getById(recordId);
    }

    @Test
    public void getAllStudents() {
        List<StudentInfo> studentInfoList = new ArrayList<>();
        when(daLayer.getAll()).thenReturn(studentInfoList);

        assertEquals(studentInfoList, studentServiceLayer.getAllStudents());
        verify(daLayer).getAll();
    }

    @Test
    public void getStudentsByGroup() {
        List<StudentInfo> studentInfoList = new ArrayList<>();
        Long groupId = 123L;
        when(daLayer.getStudentsByGroup(groupId)).thenReturn(studentInfoList);

        assertEquals(studentInfoList, studentServiceLayer.getStudentsByGroup(groupId));
        verify(daLayer).getStudentsByGroup(groupId);

    }

    @Test
    public void deleteStudent() {
        Long recordId = 123L;
        studentServiceLayer.deleteStudent(recordId);

        verify(daLayer).deleteStudentById(recordId);
    }

    @Test
    public void createRecordId() {
        Long groupId = 1L;
        Long idInGroup = 2L;
        Long studentId = 22001002L;
        assertEquals(studentId, StudentServiceLayer.createRecordId(groupId, idInGroup));
    }
}