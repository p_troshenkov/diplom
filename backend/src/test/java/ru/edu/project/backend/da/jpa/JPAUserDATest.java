package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.da.jpa.converter.UserInfoMapper;
import ru.edu.project.backend.da.jpa.entity.UserEntity;
import ru.edu.project.backend.da.jpa.repository.UserEntityRepostitory;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPAUserDATest {

    @Mock
    private UserEntityRepostitory repo;

    @Mock
    private UserInfoMapper mapper;

    @InjectMocks
    private JPAUserDA userDA;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void register() {
        UserEntity userEntity = new UserEntity();
        UserInfo userInfo = UserInfo.builder().build();

        when(mapper.map(userInfo)).thenReturn(userEntity);
        when(repo.save(userEntity)).thenReturn(userEntity);
        when(mapper.map(userEntity)).thenReturn(userInfo);

        assertEquals(userInfo, userDA.register(userInfo));
        verify(mapper).map(any(UserInfo.class));
        verify(mapper).map(any(UserEntity.class));
        verify(repo).save(any(UserEntity.class));
    }

    @Test
    public void findByUsername() {
        String username = "username";
        UserEntity userEntity = new UserEntity();
        UserInfo userInfo = UserInfo.builder().build();

        when(repo.findByUsername(username)).thenReturn(userEntity);
        when(mapper.map(userEntity)).thenReturn(userInfo);

        assertEquals(userInfo, userDA.findByUsername(username));
        verify(repo).findByUsername(username);
        verify(mapper).map(userEntity);
    }
}