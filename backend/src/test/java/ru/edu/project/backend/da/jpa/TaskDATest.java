package ru.edu.project.backend.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.tasks.RequestToMakeAssessment;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.da.jpa.entity.SubjectEntity;
import ru.edu.project.backend.da.jpa.entity.TaskEntity;
import ru.edu.project.backend.da.jpa.repository.SubjectEntityRepository;
import ru.edu.project.backend.da.jpa.repository.TaskEntityRepository;
import ru.edu.project.backend.model.Task;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class TaskDATest {

    @InjectMocks
    private TaskDA taskDA;

    @Mock
    private TaskEntityRepository taskEntityRepository;

    @Mock
    private SubjectEntityRepository subjectEntityRepository;

    @Mock
    private RequestToMakeAssessment requestToMakeAssessment;

    @Mock
    private Task taskMock;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void getByStudentParentIdNotNullTest() {

        long studentId = 123L;
        when(taskEntityRepository.findAllByStudentId(studentId)).thenAnswer(invocationOnMock -> {

            List<TaskEntity> list = new ArrayList<>();
            TaskEntity taskEntity = new TaskEntity();

            long id = 123L;
            taskEntity.setId(id);
            taskEntity.setParentId(id);
            taskEntity.setTaskSubtask("subtask");

            list.add(taskEntity);

            return list;
        });

        List<Task> list = taskDA.getByStudent(studentId);
        Task task = list.get(0).getSubTasks().get(0);

        assertEquals(task.getTaskSubtask(), "subtask");

    }

    @Test
    public void getByStudentParentIdIsNullTest() {

        long studentId = 123L;
        when(taskEntityRepository.findAllByStudentId(studentId)).thenAnswer(invocationOnMock -> {

            List<TaskEntity> list = new ArrayList<>();
            TaskEntity taskEntity = new TaskEntity();

            long id = 123L;
            taskEntity.setId(id);
            taskEntity.setParentId(null);
            taskEntity.setTaskSubtask("subtask");

            list.add(taskEntity);

            return list;
        });

        List<Task> list = taskDA.getByStudent(studentId);
        Task task = list.get(0);

        assertEquals(task.getTaskSubtask(), "subtask");

    }

    @Test
    public void saveTest() {

        TaskEntity draft = new TaskEntity();
        draft.setId(123L);
        draft.setStudentId(123L);
        draft.setStatus(null);
        draft.setCreatedAt(null);
        draft.setDeadLine(null);
        draft.setComment(null);
        draft.setParentId(123L);
        draft.setTaskSubtask(null);
        draft.setAssessment(null);

        when(taskEntityRepository.save(any(TaskEntity.class))).thenReturn(draft);

        Task expectedTask = taskDA.save(taskMock);

        assertEquals(expectedTask.getId(), new Long(123L));
    }

    @Test
    public void getAllTasksTest() {

        when(taskEntityRepository.findAll()).thenAnswer(invocationOnMock -> {

            List<TaskEntity> list = new ArrayList<>();
            TaskEntity taskEntity = new TaskEntity();

            long id = 123L;
            taskEntity.setId(id);
            taskEntity.setParentId(null);

            list.add(taskEntity);

            return list;
        });

        List<TaskInfo> expectedTaskInfo = taskDA.getAllTasks();

        assertTrue(expectedTaskInfo.size() > 0);
        assertEquals(expectedTaskInfo.get(0).getId(), new Long(123));
    }

    @Test
    public void createTaskTest() {

        long studentId = 123L;
        TaskForm form = TaskForm.builder()
                .studentId(studentId)
                .deadLine(null)
                .selectedSubjects(new ArrayList<Long>() {{
                    add(1L);
                }})
                .comment("comment")
                .build();


        when(subjectEntityRepository.findAll()).thenAnswer(invocationOnMock -> {

            List<SubjectEntity> list = new ArrayList<>();

            SubjectEntity subjectEntity = new SubjectEntity();
            subjectEntity.setId(123L);
            subjectEntity.setTitle("subject");
            subjectEntity.setDesc("description");
            list.add(subjectEntity);

            return list;
        });

        when(taskEntityRepository.save(any(TaskEntity.class))).thenAnswer(invocationOnMock -> {

            TaskEntity task = invocationOnMock.getArgument(0, TaskEntity.class);
            assertEquals(form.getComment(), task.getComment());

            return null;
        });

        TaskInfo expectedTaskInfo = taskDA.createTask(form);

        assertEquals(expectedTaskInfo.getId(), new Long(123L));

    }

    @Test
    public void getTaskByStudentParentIdIsNullTest() {

        Long studentId = 123L;

        when(taskEntityRepository.findAllByStudentId(studentId)).thenAnswer(invocationOnMock -> {

            List<TaskEntity> list = new ArrayList<>();
            TaskEntity taskEntity = new TaskEntity();

            long id = 123L;
            taskEntity.setId(id);
            taskEntity.setParentId(null);
            taskEntity.setComment("comment");

            list.add(taskEntity);

            return list;
        });

        TaskInfo expectedInfo = taskDA.getTaskByStudent(studentId);

        assertEquals(expectedInfo.getComment(), "comment");
    }

    @Test
    public void getTaskByStudentParentIdIsNotNullTest() {

        Long studentId = 123L;

        when(taskEntityRepository.findAllByStudentId(studentId)).thenAnswer(invocationOnMock -> {

            List<TaskEntity> list = new ArrayList<>();
            TaskEntity taskEntity = new TaskEntity();

            long id = 123L;
            taskEntity.setId(id);
            taskEntity.setParentId(id);
            taskEntity.setTaskSubtask("subject");

            list.add(taskEntity);

            return list;
        });

        TaskInfo expectedInfo = taskDA.getTaskByStudent(studentId);

        assertEquals(expectedInfo.getSubjects().get(0).getTitle(), "subject");
    }
    @Test
    public void makeAssessmentTest() {

        long studentId = 123L;
        long assessment = 2l;
        RequestToMakeAssessment request = RequestToMakeAssessment.builder()
                .studentId(studentId)
                .assessments(new Long[] {assessment})
                .build();

        List<TaskEntity> list = new ArrayList<>();

        when(taskEntityRepository.findAllByStudentId(anyLong())).thenAnswer(invocationOnMock -> {

            TaskEntity taskEntity = new TaskEntity();

            long parentId = 123L;
            taskEntity.setParentId(parentId);

            list.add(taskEntity);

            return list;
        });

        when(taskEntityRepository.save(any(TaskEntity.class))).thenAnswer(invocationOnMock -> {

            TaskEntity task = invocationOnMock.getArgument(0, TaskEntity.class);
            assertEquals(list.get(0).getAssessment(), new Long(assessment));

            return null;
        });

        TaskInfo expectedInfo = taskDA.makeAssessment(request);

        assertEquals(expectedInfo.getComment(), "empty");
    }
}