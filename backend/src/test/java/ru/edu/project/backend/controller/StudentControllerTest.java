package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.students.StudentForm;
import ru.edu.project.backend.api.students.StudentInfo;
import ru.edu.project.backend.service.StudentServiceLayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class StudentControllerTest {

    @Mock
    private StudentServiceLayer delegate;

    @InjectMocks
    private StudentController studentController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }

    @Test
    public void editStudent() {
        StudentForm studentForm = StudentForm.builder().build();
        StudentInfo studentInfo = StudentInfo.builder().build();
        when(delegate.editStudent(studentForm)).thenReturn(studentInfo);

        assertEquals(studentInfo, studentController.editStudent(studentForm));
        verify(delegate).editStudent(studentForm);
    }

    @Test
    public void getDetailedInfo() {
        Long recordId = 123L;
        StudentInfo studentInfo = StudentInfo.builder().build();
        when(delegate.getDetailedInfo(recordId)).thenReturn(studentInfo);

        assertEquals(studentInfo, studentController.getDetailedInfo(recordId));
        verify(delegate).getDetailedInfo(recordId);
    }

    @Test
    public void getAllStudents() {
        List<StudentInfo> studentInfoList = new ArrayList<>();
        when(delegate.getAllStudents()).thenReturn(studentInfoList);

        assertEquals(studentInfoList, studentController.getAllStudents());
        verify(delegate).getAllStudents();
    }

    @Test
    public void getStudentsByGroup() {
        Long groupId = 123L;
        List<StudentInfo> studentInfoList = new ArrayList<>();
        when(delegate.getStudentsByGroup(groupId)).thenReturn(studentInfoList);

        assertEquals(studentInfoList, studentController.getStudentsByGroup(groupId));
        verify(delegate).getStudentsByGroup(groupId);
    }

    @Test
    public void deleteStudent() {
        Long recordId = 123L;
        studentController.deleteStudent(recordId);

        verify(delegate).deleteStudent(recordId);
    }
}