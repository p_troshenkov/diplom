package ru.edu.project.backend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.tasks.RequestToMakeAssessment;
import ru.edu.project.backend.api.tasks.Subject;
import ru.edu.project.backend.api.tasks.TaskForm;
import ru.edu.project.backend.api.tasks.TaskInfo;
import ru.edu.project.backend.da.jpa.SubjectDA;
import ru.edu.project.backend.da.jpa.TaskDA;
import ru.edu.project.backend.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.function.ObjDoubleConsumer;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class TaskControllerTest {

    @InjectMocks
    private TaskController taskController;

    @Mock
    private TaskDA taskDA;

    @Mock
    private SubjectDA subjectDA;

    @Mock
    private TaskForm taskForm;

    @Mock
    private Task task;

    @Mock
    private RequestToMakeAssessment request;

    @Before
    public void setUp() throws Exception {

        openMocks(this);
    }

    @Test
    public void getAvailableTest() {

        when(subjectDA.getAvailable()).thenReturn(new ArrayList<Subject>());

        List<Subject> expected = taskController.getAvailable();

        assertTrue(expected instanceof ArrayList);
        verify(subjectDA).getAvailable();
    }

    @Test
    public void createTaskTest() {

        when(taskDA.createTask(taskForm)).thenReturn(TaskInfo.builder().build());

        TaskInfo expected = taskController.createTask(taskForm);

        assertTrue(expected instanceof TaskInfo);
        verify(taskDA).createTask(taskForm);
    }

    @Test
    public void getTaskByStudentTest() {

        long studentId = 123L;
        when(taskDA.getTaskByStudent(studentId)).thenReturn(TaskInfo.builder().build());

        TaskInfo expected = taskController.getTaskByStudent(studentId);

        assertTrue(expected instanceof TaskInfo);
        verify(taskDA).getTaskByStudent(studentId);
    }

    @Test
    public void getAllTasksTest() {

        when(taskDA.getAllTasks()).thenReturn(new ArrayList<>());

        List<TaskInfo> expected = taskController.getAllTasks();

        assertTrue(expected instanceof ArrayList);
        verify(taskDA).getAllTasks();
    }

    @Test
    public void makeAssessmentTest() {

        when(taskDA.makeAssessment(request)).thenReturn(TaskInfo.builder().build());

        TaskInfo expected = taskController.makeAssessment(request);

        assertTrue(expected instanceof TaskInfo);
        verify(taskDA).makeAssessment(request);
    }

    @Test
    public void get() {

        long studentId = 123L;
        when(taskDA.getByStudent(studentId)).thenReturn(new ArrayList<>());

        List<Task> expected = taskController.get(studentId);

        assertTrue(expected instanceof ArrayList);
        verify(taskDA).getByStudent(studentId);
    }

    @Test
    public void save() {

        when(taskDA.save(task)).thenReturn(Task.builder().build());

        Task expected = taskController.save(task);

        assertTrue(expected instanceof Task);
        verify(taskDA).save(task);
    }
}